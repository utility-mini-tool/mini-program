Card是一款基于ThinkPHP5+微信小程序开发的一款个人使用打卡记工的一款软件。
===============


## **主要特性**

* 使用简单，功能尚可

## **特别鸣谢**

感谢以下的项目,排名不分先后

ThinkPHP：http://www.thinkphp.cn

Bootstrap：http://getbootstrap.com

## 版权信息

个人开发使用，没有版权。

遵循Apache2开源协议发布，并提供免费使用。
