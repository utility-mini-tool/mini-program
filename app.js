//app.js
App({
  onLaunch: function () {
      wx.showLoading({ title: '系统初始化中' })

      var that = this;
      // 查看是否授权
      wx.getSetting({
          success(res) {
            if (res.authSetting['scope.userInfo']) {
                // 登录
                wx.login({
                    success: res => {
                        wx.showLoading({ title: '登陆中' })
                        // 获取用户信息，没有openid
                        wx.getUserInfo({
                            success: function (e) {
                                wx.showLoading({ title: '获取身份信息中'})
                                // 发送 res.code 到后台换取 openId, sessionKey, unionId
                                wx.request({
                                    url: that.globalData.url + 'login',
                                    data: {
                                      code: res.code, encryptedData: e.encryptedData, iv: e.iv
                                    },
                                    success(res) {
                                        if (res.data.code === 200){
                                            // 设置用户身份到全局
                                            that.globalData.userInfo = res.data.data;
                                            if (that.userInfoReadyCallback) {
                                              that.userInfoReadyCallback(res)
                                            }
                                            wx.showToast({
                                              title: res.data.msg,
                                              icon: 'none',
                                              duration: 500
                                            })
                                          
                                        }else{
                                            wx.showToast({
                                              title: res.data.msg,
                                              icon: 'none',
                                              duration: 30000
                                            })
                                        }
                                    }
                                })

                            }
                        })
                    }
                })
            }else{
                wx.hideLoading()
            }
          }
      })

    wx.request({
      url: that.globalData.url + 'islogin',
      data: {},
      success(res) {
        console.log(res.data.data)
        if (res.data.code === 200) {

          that.globalData.islogin = res.data.data;
        }
      }
    })

  },

  globalData: {
    islogin:false,
    // islogin: '/pages/index/index',
    userInfo: null,
    url:'https://www.jimuworld.cn/api/card/'
  },

  // 提交新的记录
  submit: function (record, warn, imgUrl) {
    wx.showLoading({  title: '加载中',  })
    wx.request({
      url: app.globalData.url + 'insertlog?openid=' + this.globalData.userInfo.openId,
      data: { record: record, warn: warn, imagesUrl:imgUrl },
      header: {
        'content-type': 'application/json' // 默认值
      },
      method: 'post',
      success(res) {
        var data = res.data;
        wx.hideLoading()
        wx.showToast({
          title: data.msg,
          icon: 'none',
          duration: 3000
        })
       
      }
    })
  }

})