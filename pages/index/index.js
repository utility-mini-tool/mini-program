// 引用公用app.js
var app = getApp();
Page({
  data: {
    // 记录日志上传图片
    images: [],
    // 打卡时长 10为一天 5为半天
    card_status: 0,
    // 打卡备注
    card_info: '',
    // 指定日期
    card_time: '',
    // 打卡工人id
    card_userid: 0,
    // 修改打卡记录id
    // 数据长度
    cardLength:0,
    card_id: 0,
    // 打卡类型 trur为今日打卡 else为指定日期
    card_type:0,
    // 添加用户数据
    addUserName: '',
    showAddLogModal: false,
    showLogModal: false,
    showBgModal: false,
    showAddUserModal: false,
    // 记录提醒
    cardAlert: [],
    // 用户和数据
    user:[],
    // 单inout弹窗title
    oneInputTitle:'',
    // 单inout弹窗Placeholder
    oneInputPlaceholder: '',
    // 单inout弹窗url
    oneInputUrl: '',
    // 项目id
    project:[],
    // 添加日志内容
    logInnerIput: false,
    //日志是否提醒
    switch1Input: false
  },
  // 本地选择图片或拍摄
  chooseImage(e) {
    wx.chooseImage({
      sizeType: ['original', 'compressed'],  //可选择原图或压缩后的图片
      sourceType: ['album', 'camera'], //可选择性开放访问相册、相机
      success: res => {
        const image = this.data.images.concat(res.tempFilePaths)
        this.setData({
          images: image.length <= 3 ? image : image.slice(0, 3)
        })
      }
    })
  },
  handleImagePreview(e) {
    const idx = e.target.dataset.idx
    const images = this.data.images
    wx.previewImage({
      current: images[idx],  //当前预览的图片
      urls: images,  //所有要预览的图片
    })
  },
  onLoad: function (options) {
    wx.showLoading({ title: '加载中' })
    var that = this;
    wx.request({
      url: app.globalData.url + 'cardindex?openid=' + app.globalData.userInfo.openId,
      data: {},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        if (res.data.code === 200) {
          var data = res.data.data;
          console.log(data)
          
          if (data.calendar){
            console.log(data.calendar)
            
            that.setData({
              cardLength: data.calendar[0].user.length,
              indexLogWidth: data.calendar[0].user.length * 183 + 'rpx',
              user: data.calendar
            })
          }else{
            that.setData({  user: [] })
          }
          that.setData({
            openid: app.globalData.userInfo.openId,
            cardAlert: data.cardAlert,
            project: data.project,
          })
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 1000
          })
          
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 3000
          })
        }

        

      }
    })
    
  },
  onShow() {
    console.log('onshow')
  },
  onShow() {
    console.log('onshow')
  },
  // 打卡提交
  cardoperate: function (e) {
    wx.showLoading({ title: '加载中' })
    var storeData = this.data;
    if (!storeData.card_userid || !storeData.card_time && !storeData.card_type){
      wx.showToast({
        title: '信息异常，请重新打开',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    var id = storeData.card_type ? 0 : storeData.card_id;
    var that = this;
    wx.request({
      url: app.globalData.url + 'cardoperate?openid=' + app.globalData.userInfo.openId,
      data: { 
        userid: storeData.card_userid, 
        time: storeData.card_time, 
        status: storeData.card_status, 
        id: storeData.card_id, 
        info: storeData.card_info, 
        card_type: storeData.card_type, 
        pid:storeData.project.id
      },
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        if (res.data.code == 200){
          that.close();
          that.onLoad();
          return;
        }
      }
    })
  
  },

  // 打卡类型切换
  cardType: function (e) {
    this.setData({
      card_status: e.currentTarget.id
    })
  },

  // 打卡文字描述
  cardInfoIput: function (e) {
    this.setData({
      card_info: e.detail.value
    })
  },

  // 打开记录列表页面
  openLogsPage: function (e) {
    wx.switchTab({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
      url: "/pages/logs/logs"
    })
  },

  // 打卡弹窗
  openCard: function (e) {
    var dat = e.currentTarget.dataset.index;
    if (!e.currentTarget.id || !dat.keytime && !dat.name){
      wx.showToast({
        title: '数据异常，请关闭后重新打开试试！',
        icon: 'none',
        duration: 5000
      })
      return;
    }
    var that = this;
    that.setData({
      showLogModal: true,
      showBgModal: true,
      card_time: dat.keytime ? dat.keytime : '',
      card_type: dat.name ? dat.name : 0,
      card_userid: e.currentTarget.id,
      card_id: dat.id ? dat.id : 0
    })

    // 如果是当天打卡
    if (dat.name !== undefined) {
      var lastData = that.data.user;
      for (var i = 0; i < lastData.length; i++) {
        if (lastData[i].user.id == e.currentTarget.id) {
          for (var ii = 1; ii <= that.data.cardLength; ii++) {
            that.setData({
              card_status: lastData[i].user.card[ii].status ? lastData[i].user.card[ii].status : 0,
              card_info: lastData[i].user.card[ii].info ? lastData[i].user.card[ii].info : ''
            })
          }
        }
      }

    // 如果是指定日期打卡
    }else{
      that.setData({
        card_status: dat.status ? dat.status : 0,
        card_info: dat.info ? dat.info : '',
      })
    }
  },

  // 添加记录弹窗
  openAddLog: function () {
    this.setData({
      showAddLogModal: true,
      showBgModal: true
    })
  },

  // 添加用户弹窗
  openAddUser: function () {
    this.setData({
      oneInputTitle: '添加用户',
      oneInputUrl: app.globalData.url + 'adduser?openid=' + app.globalData.userInfo.openId,
      oneInputPlaceholder: '输入要添加人员的名字',
      showAddUserModal: true,
      showBgModal:true
    })
  },

  // 添加用户用户名
  addUserIput: function (e) {
    this.setData({
      addUserName: e.detail.value
    })
  },

  // 添加单input提交 添加项目-添加用户
  adduseroperate: function (e) {
    var name = this.data.addUserName;
    if (!name) {
      wx.showToast({
        title: '信息不可以是空!',
        icon: 'none',
        duration: 2000
      })
      return;
    }
   
    wx.showLoading({  title: '加载中' })
    var that = this;
    wx.request({
      url: this.data.oneInputUrl,
      data: { name: name, pid: this.data.project.id},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'success',
          duration: 1000
        })
        if (res.data.code == 200) {
          that.close();
          that.onLoad();
          return;
        }
      }
    })

  },

  // 关闭所有弹窗
  close: function () {
    this.setData({
      showAddLogModal: false,
      showBgModal: false,
      showAddUserModal: false, showLogModal:false,
      // 清空所有打卡信息
      card_status:  0,
      card_info: '',
      card_time: '',
      card_type: 0,
      card_userid: 0,
      images: [],
      card_id: 0
    })
  },

  // 提交新的记录日志
  submit: function () {
    var data = this.data;
    if (!data.logInnerIput){
      wx.showToast({
        title: '请输入要记录的文字',
        icon: 'none',
        duration: 5000
      })
      return;
    }
    wx.showLoading({ title: '上传中' })

    //将选择的图片组成一个Promise数组，准备进行并行上传
    var nowMumber = 0;
    var that = this;
    var imgUrl = '';
    // 图片总数
    var contnum = data.images.length;
    // 如果有上传图片
    if (contnum) {
      for (let path of data.images) {
        wx.uploadFile({
          url: app.globalData.url + 'uploadFild?openid=' + app.globalData.userInfo.openId,
          filePath: path,
          name: 'logImage',
          success(res) {
            wx.hideLoading()
            var res = JSON.parse(res.data);
            if (res.code == 200) {
              nowMumber++;
              imgUrl += ',' + res.data
              // 如果是最后一个并且成功，则提交
              if (nowMumber == contnum) {
                that.addlog(data.logInnerIput, data.logInnerIput, imgUrl)
              }
            } else {
              wx.showToast({
                title: data.msg,
                icon: 'none',
                duration: 3000
              })
            }
          }
        })
      }
    } else {
      this.addlog(data.logInnerIput, data.logInnerIput)
    }
  },
  // 提交日志
  addlog: function (record, warn, imagesUrl = null) {
    // 提交服务器
    var that = this;
    wx.request({
      url: app.globalData.url + 'insertlog?openid=' + app.globalData.userInfo.openId,
      data: { record: record, warn: warn, imagesUrl: imagesUrl },
      header: {
        'content-type': 'application/json' // 默认值
      },
      method: 'post',
      success(res) {
        var data = res.data;
        if (data.code == 200) {
          wx.hideLoading()
          that.onLoad();
          that.close();
        }
        wx.showToast({
          title: data.msg,
          icon: 'none',
          duration: 3000
        })
      }
    })
    // 提交服务器结束
  },

  // 记录日志内容
  logInnerIput: function (e) {
    this.setData({
      logInnerIput: e.detail.value
    })
  },

  // 是否提醒
  switch1Change: function (e) {
    this.setData({
      switch1Input: e.detail.value
    })
  },

  // 打开项目操作
  projectoperation: function (e) {
    var that = this;
    wx.showActionSheet({
      itemList: ['添加项目', '项目列表'],
      success(res) {
        if (res.tapIndex === 0){
          that.setData({
            oneInputTitle: '添加项目',
            oneInputUrl: app.globalData.url + 'addproject?openid=' + app.globalData.userInfo.openId,
            oneInputPlaceholder: '输入名字和介绍，方便以后自己看',
            showAddUserModal: true,
            showBgModal: true
          })
        } else if (res.tapIndex === 1){
          wx.navigateTo({
            url: '/pages/project/project'
          })
        }
      }
    })
  },
})