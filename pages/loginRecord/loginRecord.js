
var app = getApp();
Page({
  data: {
    logs: []
  },
  onLoad: function () {
    wx.showLoading({ title: '加载中' })
    var that = this;
    wx.request({
      url: app.globalData.url + 'loginRecord?openid=' + app.globalData.userInfo.openId,
      data: {},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        if (res.data.code === 200) {
          var data = res.data.data;
          that.setData({
            loginData: data
          })
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 1000
          })
        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 3000
          })
        }
      }
    })
  }
})