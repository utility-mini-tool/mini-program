
var app = getApp();
Page({
  data: {
    // 记录日志上传图片
    images: [],
    logs:[],
    // 禁止点击
    forbid:true,
    searchIput:'',
    logInnerIput:'',
    switch1Input:false,
    showAddLogModal: false,
    showBgModal:false,
  },
  onLoad: function () {
    wx.showLoading({  title: '加载中' })
    var that = this;
    wx.request({
      url: app.globalData.url + 'logslist?openid=' + app.globalData.userInfo.openId,
      data: {},
      header: {
        'content-type': 'application/json'
      },
      success(res) {
        wx.hideLoading()
        if (res.data.code === 200) {
            var data = res.data.data;
            that.setData({
              logs: data
            })
            wx.showToast({
              title: res.data.msg,
              icon: 'success',
              duration: 500
            })
        }else{
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 2000
          })
        }
      }
    })
  },
  // 本地选择图片或拍摄
  chooseImage(e) {
    wx.chooseImage({
      sizeType: ['original', 'compressed'],  //可选择原图或压缩后的图片
      sourceType: ['album', 'camera'], //可选择性开放访问相册、相机
      success: res => {
        const image = this.data.images.concat(res.tempFilePaths)
        this.setData({
          images: image.length <= 3 ? image : image.slice(0, 3)
        })
      }
    })
  },
  handleImagePreview(e) {
    const idx = e.target.dataset.idx
    const images = e.target.dataset.index
    wx.previewImage({
      current: images[idx],  //当前预览的图片
      urls: images,  //所有要预览的图片
    })
  },
  // 打开添加记录弹窗
  openAddLog: function () {
    this.setData({
      showAddLogModal: true,
      showBgModal: true
    })
  },
  // 关闭弹窗
  close: function () {
    this.setData({
      showAddLogModal: false,
      showBgModal: false,
      images:[]
    })
  },
  // 编辑记录
  del: function (e) {
    console.log(e.currentTarget.id)
    var that = this;
    wx.showActionSheet({
      itemList: ['删除', '添加提醒', '取消提醒'],
      success(res) {
        // 如果是删除，则确认
        if (res.tapIndex == 0){
          wx.showModal({
            title: '提示',
            content: '确定删除这条记录吗？',
            success(res) {
              if (res.confirm) {

                // 删除操作开始
                wx.showLoading({  title: '删除中' })
               
                wx.request({
                  url: app.globalData.url + 'dellog?openid=' + app.globalData.userInfo.openId,
                  data: { id: e.currentTarget.id },
                  header: { 'content-type': 'application/json' },
                  method:'post',
                  success(res) {
                    var data = res.data
                    wx.hideLoading()
                    wx.showToast({
                      title: data.msg,
                      icon: 'none',
                      duration: 500
                    })
                    that.onLoad();
                  }
                })
                // 删除结束
              } else if (res.cancel) {
                return;
              }
            }
          })
        }else{

          // 修改状态
          wx.showLoading({ title: '请稍后' })
          wx.request({
            url: app.globalData.url + 'updatelogstatus?openid=' + app.globalData.userInfo.openId,
            data: { id: e.currentTarget.id, warn: res.tapIndex},
            header: { 'content-type': 'application/json' },
            method: 'post',
            success(res) {
              var data = res.data
              if (data.code == 200) {
                wx.hideLoading()
              }
              wx.showToast({
                title: data.msg,
                icon: 'none',
                duration: 500
              })
              that.onLoad();
            }
          })

        }
        
      }
    })
  }, 
  // 提交新的记录日志
  submit: function () {
    var data = this.data;
    if (!data.logInnerIput) {
      wx.showToast({
        title: '请输入要记录的文字',
        icon: 'none',
        duration: 5000
      })
      return;
    }
    wx.showLoading({ title: '上传中' })

    //将选择的图片组成一个Promise数组，准备进行并行上传
    var nowMumber = 0;
    var that = this;
    var imgUrl = '';
    // 图片总数
    var contnum = data.images.length;
    // 如果有上传图片
    if (contnum){
        for (let path of data.images) {
          wx.uploadFile({
            url: app.globalData.url + 'uploadFild?openid=' + app.globalData.userInfo.openId,
            filePath: path,
            name: 'logImage',
            success(res) {
              wx.hideLoading()
              var res = JSON.parse(res.data);
              if (res.code == 200) {
                nowMumber++;
                imgUrl += ',' + res.data
                // 如果是最后一个并且成功，则提交
                if (nowMumber == contnum) {
                  that.addlog(data.logInnerIput, data.logInnerIput, imgUrl)
                }
              } else {
                wx.showToast({
                  title: data.msg,
                  icon: 'none',
                  duration: 3000
                })
              }
            }
          })
        }
    }else{
      this.addlog(data.logInnerIput, data.logInnerIput)
    }
    
  },
  addlog: function (record, warn, imagesUrl = null){
    // 提交服务器
    var that = this;
    wx.request({
      url: app.globalData.url + 'insertlog?openid=' + app.globalData.userInfo.openId,
      data: { record: record, warn: warn, imagesUrl: imagesUrl },
      header: {
        'content-type': 'application/json' // 默认值
      },
      method: 'post',
      success(res) {
        var data = res.data;
        if (data.code == 200) {
          wx.hideLoading()
          that.onLoad();
          that.close();
        }
        wx.showToast({
          title: data.msg,
          icon: 'none',
          duration: 3000
        })
      }
    })
     // 提交服务器结束
  },
  // 获取输入的内容
  logInnerIput: function (e) {
    this.setData({
      logInnerIput: e.detail.value
    })
  }, 
  // 获取搜索内容
  searchIput: function (e) {
    this.setData({
      searchIput: e.detail.value
    })
  }, 
  // 搜索提交
  seachSubmit: function () {
    var keyword = this.data.searchIput;
    if (!keyword) {
      wx.showToast({
        title: '请输入内容！',
        icon: 'none',
        duration: 2000
      })
      return;
    }
    this.setData({  forbid: false })
    wx.showLoading({ title: '搜索中...' })
    var that = this;
    wx.request({
      url: app.globalData.url + 'searchLogs?openid=' + app.globalData.userInfo.openId,
      data: { keyword: keyword },
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        wx.showToast({
          title: res.data.msg,
          icon: 'none',
          duration: 2000
        })
        that.setData({ forbid: true })
        if (res.data.code == 200) {
          var data = res.data.data;
          console.log(data)
          
          that.setData({
            logs: data
          })
          return;
        }
      }
    })

  },
  // 是否提醒
  switch1Change: function (e) {
    this.setData({
      switch1Input: e.detail.value
    })
  },
})
