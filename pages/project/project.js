// 引用公用app.js
var app = getApp();
Page({
  data: {
    project: []
  }, 
  onLoad: function (options) {
    wx.showLoading({ title: '加载中' })
    var that = this;
    wx.request({
      url: app.globalData.url + 'project?openid=' + app.globalData.userInfo.openId,
      data: {},
      method: 'post',
      header: { 'content-type': 'application/json' },
      success(res) {
        wx.hideLoading()
        if (res.data.code === 200) {
          that.setData({
            project: res.data.data
          })
          wx.showToast({
            title: res.data.msg,
            icon: 'success',
            duration: 1000
          })

        } else {
          wx.showToast({
            title: res.data.msg,
            icon: 'none',
            duration: 3000
          })
        }
      }
    })

  }
})