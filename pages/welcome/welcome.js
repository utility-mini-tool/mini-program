//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
    motto: '记工 & 笔记',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onLoad: function () {
    
    var that = this;
    wx.getSetting({
      success: res => {
        wx.showLoading({ title: '检测状态' })
        // console.log(res.authSetting['scope.userInfo'])
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
            if (app.globalData.userInfo) {
              if(app.globalData.islogin){
                // 跳转首页
                wx.showLoading({ title: '跳转首页中' })
                wx.reLaunch({  url: '/pages/index/index' })
                that.setData({
                  userInfo: app.globalData.userInfo,
                  hasUserInfo: true
                })
              }else{
                 wx.showLoading({ title: '登陆成功，跳转首页中' })
                wx.navigateTo({
                    url: '/pages/test/test'
                  })
              }
              
            } else if (this.data.canIUse) {
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回 所以此处加入 callback 以防止这种情况
              app.userInfoReadyCallback = res => {
                // 如果登陆成功 跳转到首页
                if (app.globalData.userInfo){
                  if(app.globalData.islogin){
                    wx.showLoading({ title: '登陆成功，跳转首页中' })
                    wx.reLaunch({  url: '/pages/index/index'  })
                  }else{
                    wx.showLoading({ title: '登陆成功，跳转首页中' })
                    wx.navigateTo({
                      url: '/pages/test/test'
                    })
                  }
                }
              }
            } else {
              // 在没有 open-type=getUserInfo 版本的兼容处理
              wx.getUserInfo({
                success: res => {
                  app.globalData.userInfo = res.userInfo
                  that.setData({
                    userInfo: res.userInfo,
                    hasUserInfo: true
                  })
                }
              })
            }

        // 没有授权
        }else{
          wx.hideLoading()
          wx.showToast({
            title: '请点击登陆',
            icon: 'none',
            duration: 1000
          })
        }
      }
    })
    
  },
  getUserInfo: function (e) {
    app.onLaunch()
    this.onLoad()
  },
  openIndex: function (e) {
    if (app.globalData.islogin){
      wx.reLaunch({
        url: '/pages/index/index'
      })
    }
   
  
  }
})
